package croclabs.time;

import java.util.concurrent.TimeUnit;

public class Duration {
	private Duration() {
		super();
	}

	public static String millisToMinSecs(long millis) {
		return String.format(
				"%d m, %d s",
				TimeUnit.MILLISECONDS.toMinutes(millis),
				TimeUnit.MILLISECONDS.toSeconds(millis) -
						TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
		);
	}

	public static String millisToMinSecsMillis(long millis) {
		long mins = TimeUnit.MILLISECONDS.toMinutes(millis);
		long secs = TimeUnit.MILLISECONDS.toSeconds(millis) -
				TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));
		long ms = millis -
				TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(millis));

		return String.format(
				"%d m, %d s, %d ms",
				mins,
				secs,
				ms
		);
	}

	public static String millisToHrsMinSecsMillis(long millis) {
		long hrs = TimeUnit.MILLISECONDS.toHours(millis);
		long mins = TimeUnit.MILLISECONDS.toMinutes(millis) -
				TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis));
		long secs = TimeUnit.MILLISECONDS.toSeconds(millis) -
				TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis));
		long ms = millis -
				TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(millis));

		return String.format(
				"%d h, %d m, %d s, %d ms",
				hrs,
				mins,
				secs,
				ms
		);
	}
}
