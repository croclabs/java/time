package croclabs.time;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DurationTest {

	@Test
	protected void millisToMinSecs() {
		assertEquals("3 m, 2 s", Duration.millisToMinSecs(182000));
	}

	@Test
	protected void millisToMinSecsMillis() {
		assertEquals("3 m, 2 s, 100 ms", Duration.millisToMinSecsMillis(182100));
	}

	@Test
	protected void millisToHrsMinSecsMillis() {
		assertEquals("1 h, 3 m, 2 s, 100 ms", Duration.millisToHrsMinSecsMillis(3782100));
	}
}